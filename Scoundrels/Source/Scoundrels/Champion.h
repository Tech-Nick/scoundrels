// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Champion.generated.h"

UCLASS()
class SCOUNDRELS_API AChampion : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AChampion();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
private:
	void MoveForward(float Value);
	void MoveRight(float Value);
	FVector ForwardVelocity;
	FVector RightVelocity;

};
