// Fill out your copyright notice in the Description page of Project Settings.


#include "GameFramework/PlayerController.h"
#include "Champion.h"
#include "Engine/World.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"

// Sets default values
AChampion::AChampion()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AChampion::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AChampion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Controller != NULL)
	{


		FVector ForwardTranslation = ForwardVelocity * 50 * DeltaTime;
		FVector RightTranslation = RightVelocity * 50 * DeltaTime;

		AddActorWorldOffset(ForwardTranslation, true);
		AddActorWorldOffset(RightTranslation, true);

		//store the mouse variables
		FVector mouseLocation, mouseDirection;

		//get the player controller
		APlayerController* PController = GetWorld()->GetFirstPlayerController()->GetWorld()->GetFirstPlayerController();

		//get the mouse x and y in 2d
		PController->DeprojectMousePositionToWorld(mouseLocation, mouseDirection);


		//current char rotation
		FRotator currentCharacterRotation = this->GetActorRotation();

		//Target rotation
		FRotator targetRotation = mouseDirection.Rotation();

		//set the character rotation.yaw to the target rotation .yaw
		FRotator newRotation = FRotator(currentCharacterRotation.Pitch, targetRotation.Yaw, currentCharacterRotation.Roll);

		

			this->SetActorRotation(newRotation);
		
		
		



		

	}
}

// Called to bind functionality to input
void AChampion::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AChampion::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AChampion::MoveRight);
}

void AChampion::MoveForward(float Value)
{


	
	ForwardVelocity = FVector(1, 0, 0) * 10 * Value;
}

void AChampion::MoveRight(float Value)
{
	RightVelocity = FVector(0, 1, 0) * 10 * Value;
}